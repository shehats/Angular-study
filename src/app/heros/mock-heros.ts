import { Hero } from './Hero';

export const HEROES: Hero[] = [
  new Hero(1, 'FireStorm'),
  new Hero(2, 'The Flash'),
  new Hero(3, 'Green Arrow'),
  new Hero(4, 'Batman'),
  new Hero(5, 'Sups'),
  new Hero(6, 'Wonder Woman'),
  new Hero(7, 'Harley Quinn'),
  { id: 11, name: 'Mr. Nice' },
  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
]